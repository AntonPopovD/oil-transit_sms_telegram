unit uFormMain;

interface

uses
  uDMServer,

  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, System.IniFiles;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Memo1: TMemo;
    Button5: TButton;
    Button6: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
  private
    { Private declarations }
    FConnector: TConnector;
    FOzmaConnector: TOzmaConnector;
    FiniSettings: TIniFile;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  FConnector.CheckAndSendAllMessages;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  FConnector.CheckAndSendOppsTomorrow(True);
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  FConnector.CheckAndChangeCategoryes(True);
end;

procedure TForm1.Button4Click(Sender: TObject);
var
  lMessages: TMessages;
  lEncode: TEncoding;
begin
  lEncode := TEncoding.Create;
  try
    Memo1.Lines.Add(FConnector.ReadFromIniFile('main', 'MessageDelay', '�� ���������� ���������'));
    Memo1.Lines.Add(StringOf(lEncode.Convert(lEncode.UTF8, lEncode.GetEncoding(1251)
      , BytesOf(FiniSettings.ReadString('main', 'MessageDelay'
      , '��������,%s,����� �� ����� ��������� �� ������, ������?')))));
    lMessages := FOzmaConnector.GetAllMessages;
    if Length(lMessages) > 0 then
    begin
      Memo1.Lines.Add(lMessages[0].Text);
    end;
  finally
    lEncode.Free;
  end;
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
  FConnector.CheckAndExecDelayOpps;
end;

procedure TForm1.Button6Click(Sender: TObject);
begin
  FConnector.CheckAndSendLostClients;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  FConnector := TConnector.Create;
  FOzmaConnector := TOzmaConnector.Create;
  FiniSettings := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'Settings.ini');
  DMConnector := TDMConnector.Create(Application);
  DMConnector.Timer1.Enabled := False;
  DMConnector.tmrSendSMSTomorrow.Enabled := False;
  DMConnector.tmrDelay.Enabled := False;
  DMConnector.tmrChangeCategory.Enabled := False;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  FConnector.Free;
  FOzmaConnector.Free;
  FiniSettings.Free;
end;

end.
