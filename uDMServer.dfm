object DMConnector: TDMConnector
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 236
  Width = 314
  object Timer1: TTimer
    Enabled = False
    Interval = 15000
    OnTimer = Timer1Timer
    Left = 100
    Top = 43
  end
  object tmrSendSMSTomorrow: TTimer
    Enabled = False
    Interval = 5000
    OnTimer = tmrSendSMSTomorrowTimer
    Left = 100
    Top = 93
  end
  object tmrDelay: TTimer
    Enabled = False
    OnTimer = tmrDelayTimer
    Left = 47
    Top = 41
  end
  object tmrChangeCategory: TTimer
    Enabled = False
    OnTimer = tmrChangeCategoryTimer
    Left = 183
    Top = 62
  end
end
