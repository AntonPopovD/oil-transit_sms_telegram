﻿unit uDMServer;

interface

uses
  CustIdHTTP,

  System.SysUtils, System.Classes, REST.Json, System.JSON, System.IniFiles, IdURI,
  Vcl.ExtCtrls, System.Net.URLClient, System.NetEncoding, System.Net.HttpClient,
  System.Net.HttpClientComponent, System.DateUtils, System.Generics.Collections,
  IdBaseComponent, IdComponent, IdCustomTCPServer, IdCustomHTTPServer,
  IdHTTPServer, IdContext, Vcl.Dialogs, System.IOUtils;

type
  TOpportunityInfo = record
    ID: Int64;
    CurrVolume: Integer;
    VolumePrice: Currency;
    Operation: string;
    PaymentTool: string;
    Phone: string;
  end;

  TOpportunities = array of TOpportunityInfo;

  TMessageInfo = record
    ID: Int64;
    Phone: string;
    Text: string;
    Date: TDate;
  end;

  TMessages = array of TMessageInfo;

  TLossClient = record
    FIO: string;
    ID: Int64;
    OrgName: string;
    Phone: string;
    Category: string;
    CategoryType: string;
    CurrVolume: Integer;
    Price: Currency;
    DayLoss: Integer;
  end;

  TLossClients = array of TLossClient;

  TDelayOpportunity = record
    OppID: Int64;
    OrgID: Int64;
    OrgName: string;
    OrgShortName: string;
    OrgCategory: string;
    OrgCategoryType: string;
    PersonName: string;
    PersonPhone: string;
    MinutesDelay: Integer;
    DelayTask: Int64;
  end;

  TDelayOpportunities = array of TDelayOpportunity;

  TOzmaConnector = class;

  TOzmaConnector = class
  strict private
    const cnstURL = 'https://oil-transit.api.ozma.org';
    const cnstAuthURL = 'https://account.ozma.io';
    const cnstSecret = '167bbd3f-6e38-47f2-9c1b-3d3c5bdb4b93';
    const cnstClientID = 'oil-transit';
    const cnstUsername = 'poiree@bk.ru';
    const cnstPassword = 'fotofinish';

    function SendInsertTransaction(AObject: TJSONObject): Int64;
    function SendUpdateTransaction(AObject: TJSONObject): Boolean;
  private
    FHTTP: TCustIdHTTP;
    FIsAuthorize: Boolean;
    FCurrentToken: string;
    FRefreshToken: string;
    FLastDateAuth: TDateTime;
    FMessages: TMessages;

    function TransformPhone(ASourcePhone: string): string;

    function ImportNewTokens(AJSON: string): Boolean;
  public
    constructor Create;
    destructor Destroy; override;

    procedure AddContactsFromFile(AFilename: string);
    function Authorize: Boolean;
    procedure Unauthorize;
    function GetAllMessages: TMessages;
    function GetMessagesForClientTomorrow: TOpportunities;
    procedure MessageCheckSending(AMessageID: Int64);
    function GetLossClients: TLossClients;
    procedure SetLossClientNow(AClientID: Int64);
    function GetDelayOpportunities: TDelayOpportunities;
    procedure ResendOpportunityTomorrow(AOppID: Int64; ADelayTaskID: Int64 = -1);
    procedure CreateDelayTask(AOrgID: Int64; AOppID: Int64; APriority: Integer);
    procedure CreateTaskByLostClient(AOrgID: Int64; AOrgName: string);
    procedure GetAndUpdateCategoryForClients;

    property CurrentToken: string read FCurrentToken;
  end;

  TConnector = class
  strict private
    const cnstURLSMS = 'https://sms.ru/sms/send?api_id=AD787AC4-FB99-080B-83D1-02D8BDF2E445&to=%s&msg=%s&json=1';

    const cnstSecterTokenTelegram = '1884675840:AAFgB2_ZPp7SL3SFysGHVD2XwveKKaHtZRA';
    const cnstBotName = 'oil_transit_bot';
    const cnstBotUserName = 'oil_transit';
    const cnstURLTelegram = 'https://api.telegram.org/bot%s/';
    const cnstURLFileTelegram = 'https://api.telegram.org/file/bot%s/';
    const cnstChatID = 'TestGroup';
  private
    FHTTP: TCustIdHTTP;
    FiniSettings: TIniFile;
    FOzmaConnector: TOzmaConnector;

    procedure SetLastUpdateID(const Value: Int64);
    procedure SetLastSMSDate(const Value: TDate);
    procedure SetLastCategoryesDate(const Value: TDate);

    function GetLastUpdateID: Int64;
    function GetURLFileFromHosting(AFileData: TBytesStream): string;
    function GetLastSMSDate: TDate;
    function GetLastCategoryesDate: TDate;
  public
    constructor Create;
    destructor Destroy; override;

    function ReadFromIniFile(ASection, AParam, ATextDef: string): string;
    procedure CheckAndSendAllMessages;
    procedure CheckAndSendOppsTomorrow(AForceSend: Boolean = False);
    procedure CheckAndExecDelayOpps;
    procedure CheckAndSendLostClients;
    procedure CheckAndChangeCategoryes(AForceSend: Boolean = False);

    procedure SendMessageToChat(AText: string);
    procedure SendSMS(APhone, AText: string);

//    function

    property LastSMSDate: TDate read GetLastSMSDate write SetLastSMSDate;
    property LastCategoryesDate: TDate read GetLastCategoryesDate write SetLastCategoryesDate;
    property LastUpdateID: Int64 read GetLastUpdateID;
  end;

  TDMConnector = class(TDataModule)
    Timer1: TTimer;
    tmrSendSMSTomorrow: TTimer;
    tmrDelay: TTimer;
    tmrChangeCategory: TTimer;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure tmrSendSMSTomorrowTimer(Sender: TObject);
    procedure tmrDelayTimer(Sender: TObject);
    procedure tmrChangeCategoryTimer(Sender: TObject);
  private
    FConnector: TConnector;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DMConnector: TDMConnector;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TConnector }

procedure TConnector.CheckAndChangeCategoryes(AForceSend: Boolean);
var
  lNow: TDate;
  dtFS: TFormatSettings;
begin
  dtFS.DateSeparator := '-';
  lNow := Trunc(Now);
  if (((DayOf(lNow) = 1) and (MonthOf(lNow) in [1, 3, 5, 7, 9, 11])) and (lNow > LastCategoryesDate))
    or AForceSend then
  begin
    LastCategoryesDate := lNow;
    FOzmaConnector.GetAndUpdateCategoryForClients;
  end;
end;

procedure TConnector.CheckAndExecDelayOpps;
var
  lOpportunities: TDelayOpportunities;
  lEncode: TEncoding;
  i: Integer;
  lText, lFormatText: string;
  lDelayTask: Int64;
begin
  try
//    if (LastSMSDate < DateOf(Now)) then
//    begin
    lOpportunities := FOzmaConnector.GetDelayOpportunities;
    if Length(lOpportunities) > 0 then
    begin
      lEncode := TEncoding.Create;
      try
        for i := 0 to Length(lOpportunities) - 1 do
        begin
          try
            if lOpportunities[i].OrgCategoryType <> 'Личное общение' then
            begin
              lFormatText := ReadFromIniFile('main', 'MessageDelay'
                , 'Извините,%s,вывоз бу масла перенесен на завтра, удобно?');
              lText := Format(lFormatText, [lOpportunities[i].PersonName]);
              SendSMS(lOpportunities[i].PersonPhone, lText);
              lDelayTask := -1;
              if lOpportunities[i].OrgCategoryType = 'СМС' then
              begin
                lDelayTask := 22;
              end;
              FOzmaConnector.ResendOpportunityTomorrow(lOpportunities[i].OppID, lDelayTask);
              if (lOpportunities[i].OrgCategoryType = 'СМС/ ЛО') and (lOpportunities[i].DelayTask = 0) then
              begin
                FOzmaConnector.CreateDelayTask(lOpportunities[i].OrgID, lOpportunities[i].OppID, 2);
                SendMessageToChat('Заявка ' + lOpportunities[i].OrgShortName + ' ('
                  + lOpportunities[i].OrgName + ')' + ' категории ' + lOpportunities[i].OrgCategory
                  + ' не выполнена! Отправлена СМС клиенту и создана задача для созвона. Заявка перенесена на завтра');
              end;
            end
            else
            if (lOpportunities[i].DelayTask = 0) and (lOpportunities[i].MinutesDelay <= 20) then
            begin
              FOzmaConnector.CreateDelayTask(lOpportunities[i].OrgID, lOpportunities[i].OppID, 3);
              SendMessageToChat('ВНИМАНИЕ!!! Заявка ' + lOpportunities[i].OrgShortName + ' ('
                + lOpportunities[i].OrgName + ')' + ' категории ' + lOpportunities[i].OrgCategory
                + ' не выполнена! Срочно свяжитесь с клиентом ' + lOpportunities[i].PersonName
                + '. Тел: ' + lOpportunities[i].PersonPhone);
            end
            else
            if (lOpportunities[i].MinutesDelay > 20) then
            begin
              FOzmaConnector.ResendOpportunityTomorrow(lOpportunities[i].OppID);
              SendMessageToChat('Заявка ' + lOpportunities[i].OrgShortName + ' ('
                + lOpportunities[i].OrgName + ')' + ' категории ' + lOpportunities[i].OrgCategory
                + ' просрочена и перенесена на завтра автоматически! В течении 20 минут заявку не успели обработать');
            end;
          except
            FreeAndNil(FHTTP);
            FreeAndNil(FOzmaConnector);
            Sleep(1000);
            FHTTP := TCustIdHTTP.Create(nil);
            FOzmaConnector := TOzmaConnector.Create;
          end;
        end;
      finally
        lEncode.Free;
      end;
    end;
//    end;
  except
    FreeAndNil(FOzmaConnector);
    FOzmaConnector := TOzmaConnector.Create;
  end;
end;

procedure TConnector.CheckAndSendAllMessages;
var
  lMessages: TMessages;
  i: Integer;
  lURL: string;
  lRes: string;
begin
  try
    lMessages := FOzmaConnector.GetAllMessages;
    if Length(lMessages) > 0 then
    begin
      for i := 0 to Length(lMessages) - 1 do
      begin  
        try
          FOzmaConnector.MessageCheckSending(lMessages[i].ID);
          if lMessages[i].Phone <> '' then
          begin
            SendSMS(lMessages[i].Phone, lMessages[i].Text);
//            lURL := Format(cnstURLSMS, [lMessages[i].Phone, lMessages[i].Text]);
//            lRes := FHTTP.Get(lURL);
          end
          else
          begin
            SendMessageToChat(lMessages[i].Text);
          end;
        except
          FreeAndNil(FHTTP);
          FreeAndNil(FOzmaConnector); 
          Sleep(1000);
          FHTTP := TCustIdHTTP.Create(nil);  
          FOzmaConnector := TOzmaConnector.Create;
        end;
      end;
    end;
  except
    FreeAndNil(FOzmaConnector);
    FOzmaConnector := TOzmaConnector.Create;
  end;
end;

procedure TConnector.CheckAndSendLostClients;
var
  lLossClient: TLossClients;
  lEncode: TEncoding;
  i: Integer;
  lTonn: Double;
  lFormatText, lText, lVolumeStr: string;
begin
  try
    if (LastSMSDate < DateOf(Now)) then
    begin
      lLossClient := FOzmaConnector.GetLossClients;
      if Length(lLossClient) > 0 then
      begin
        lEncode := TEncoding.Create;
        try
          for i := 0 to Length(lLossClient) - 1 do
          begin
            try
              lFormatText := ReadFromIniFile('main', 'MessageLoss'
                , 'Здравствуйте,%s,по прежнему покупаем бу масло %sр/л>%s');
              lVolumeStr := lLossClient[i].CurrVolume.ToString + 'л';
              if lLossClient[i].CurrVolume >= 1000 then
              begin
                lTonn := (lLossClient[i].CurrVolume / 1000);
                lVolumeStr := FloatToStr(lTonn) + 'т';
              end;
              lText := Format(lFormatText, [lLossClient[i].FIO, CurrToStr(lLossClient[i].Price)
                , lVolumeStr]);
              SendSMS(lLossClient[i].Phone, lText);
//              FOzmaConnector.CreateTaskByLostClient(lLossClient[i].ID, lLossClient[i].OrgName);
              SendMessageToChat('Потерянный клиент: ' + lLossClient[i].OrgName);
              FOzmaConnector.SetLossClientNow(lLossClient[i].ID);
            except
              FreeAndNil(FHTTP);
              FreeAndNil(FOzmaConnector);
              Sleep(1000);
              FHTTP := TCustIdHTTP.Create(nil);
              FOzmaConnector := TOzmaConnector.Create;
            end;
          end;
        finally
          lEncode.Free;
        end;
      end;
    end;
  except
    FreeAndNil(FOzmaConnector);
    FOzmaConnector := TOzmaConnector.Create;
  end;
end;

procedure TConnector.CheckAndSendOppsTomorrow(AForceSend: Boolean);
var
  lOpportunities: TOpportunities;
  i: Integer;   
  lURL: string;
  lEncode: TEncoding;
  lText, lFormatText: string;
  lCurrFS: TFormatSettings;
  lVolumeStr: string;
  lTonn: Float32;
  lPriceL: Float32;
  lPriceLStr: string;
begin
  try
    if (LastSMSDate < DateOf(Now)) or AForceSend then
    begin
      lOpportunities := FOzmaConnector.GetMessagesForClientTomorrow;
      if Length(lOpportunities) > 0 then
      begin
        lCurrFS.ThousandSeparator := ' ';
        lCurrFS.DecimalSeparator := '.';
        lEncode := TEncoding.Create;
        try
          for i := 0 to Length(lOpportunities) - 1 do
          begin
            try
              lFormatText := ReadFromIniFile('main', 'MessageTomorrow'
                , 'Завтра вывоз бу масла >%s/%dр ам Исузу В039КР178.Спасибо');
//              lFormatText := FiniSettings.ReadString('main', 'MessageTomorrow'
//                , 'Завтра вывоз бу масла >%s/%dр ам Исузу В039КР178.Спасибо');
//              lFormatText := StringOf(lEncode.Convert(lEncode.UTF8, lEncode.GetEncoding(1251)
//                , BytesOf(FiniSettings.ReadString('main', 'MessageTomorrow'
//                , 'Завтра вывоз бу масла >%s/%dр ам Исузу В039КР178.Спасибо'))));
              lVolumeStr := lOpportunities[i].CurrVolume.ToString + 'л';
              if lOpportunities[i].CurrVolume >= 1000 then
              begin
                lTonn := (lOpportunities[i].CurrVolume / 1000);
                lVolumeStr := FloatToStr(lTonn) + 'т';
              end;
              lPriceL := Trunc(lOpportunities[i].VolumePrice / lOpportunities[i].CurrVolume);
              lPriceLStr := FloatToStr(lPriceL);
              lText := Format(lFormatText, [lVolumeStr, lPriceLStr]);
              SendSMS(lOpportunities[i].Phone, lText);
            except
              FreeAndNil(FHTTP);
              FreeAndNil(FOzmaConnector);
              Sleep(1000);
              FHTTP := TCustIdHTTP.Create(nil);
              FOzmaConnector := TOzmaConnector.Create;
            end;
          end;
        finally
          lEncode.Free;
        end;
      end;
    end;
  except
    FreeAndNil(FOzmaConnector);
    FOzmaConnector := TOzmaConnector.Create;
  end;
end;

constructor TConnector.Create;
begin
  FHTTP := TCustIdHTTP.Create(nil);
  FiniSettings := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'Settings.ini');
  FOzmaConnector := TOzmaConnector.Create;
end;

destructor TConnector.Destroy;
begin
  FHTTP.Free;
  FiniSettings.Free;
  FOzmaConnector.Free;
  inherited;
end;

function TConnector.GetLastCategoryesDate: TDate;
begin
  Result := FiniSettings.ReadInteger('Main', 'LastCatDate', 0);
end;

function TConnector.GetLastSMSDate: TDate;
begin
  Result := FiniSettings.ReadInteger('Main', 'LastSMSDate', 0);
end;

function TConnector.GetLastUpdateID: Int64;
begin
  Result := FiniSettings.ReadInteger('Main', 'LastUpdateId', 0);
end;

function TConnector.GetURLFileFromHosting(AFileData: TBytesStream): string;
begin

end;

function TConnector.ReadFromIniFile(ASection, AParam, ATextDef: string): string;
begin
  Result := Utf8ToAnsi(FiniSettings.ReadString(ASection, AParam
    , 'Извините,%s,вывоз бу масла перенесен на завтра, удобно?'));
end;

procedure TConnector.SendMessageToChat(AText: string);
var
  lGroup: string;
  lURL, lResp: string;
begin
  lGroup := FiniSettings.ReadString('Main', 'GroupID', '');
//  ShowMessage(lGroup);
  if lGroup <> '' then
  begin
    lURL := Format(cnstURLTelegram, [cnstSecterTokenTelegram])
      + 'sendMessage?chat_id=' + lGroup + '&text=' + AText;
//    System.IOUtils.TFile.WriteAllText(ExtractFilePath(ParamStr(0)) + 'log.log', lURL);
    lURL := TIdURI.URLEncode(lURL);
//    if not (FileExists(ParamStr(0) + 'log.log')) then
//    begin
//      TFile.Create(ParamStr(0) + 'log.log').Free;
//    end;
//    System.IOUtils.TFile.WriteAllText(ExtractFilePath(ParamStr(0)) + 'log.log', lURL);
    lResp := FHTTP.Get(lURL);
  end;
end;

procedure TConnector.SendSMS(APhone, AText: string);
var
  lText, lURL: string;
  lCount: Integer;
  lEncode: TEncoding;
begin
  if APhone <> '' then
  begin
    lEncode := TEncoding.Create;
    try
  //    lText := StringOf(lEncode.Convert(lEncode.Default, lEncode.UTF8, BytesOf(AText)));
      lText := StringReplace(AText, ' ', '+', [rfReplaceAll]);
      lURL := Format(cnstURLSMS, [APhone, lText]);
      lURL := TIdURI.URLEncode(lURL);
      lCount := 1;
      while lCount < 3 do
      begin
        try
          FHTTP.Get(lURL);
          Break;
        except
          FreeAndNil(FHTTP);
          FreeAndNil(FOzmaConnector);
          Inc(lCount);
          Sleep(1000);
          FHTTP := TCustIdHTTP.Create(nil);
          FOzmaConnector := TOzmaConnector.Create;
        end;
      end;
    finally
      lEncode.Free;
    end;
  end;
end;

procedure TConnector.SetLastCategoryesDate(const Value: TDate);
var
  lValInt: Integer;
begin
  lValInt := Trunc(Value);
  FiniSettings.WriteInteger('Main', 'LastCatDate', lValInt);
end;

procedure TConnector.SetLastSMSDate(const Value: TDate);
var
  lValInt: Integer;
begin
  lValInt := Trunc(Value);
  FiniSettings.WriteInteger('Main', 'LastSMSDate', lValInt);
end;

procedure TConnector.SetLastUpdateID(const Value: Int64);
begin
  FiniSettings.WriteInteger('Main', 'LastUpdateId', Value);
end;

procedure TDMConnector.DataModuleCreate(Sender: TObject);
begin
  FConnector := TConnector.Create;
  Timer1.Enabled := True;
  tmrSendSMSTomorrow.Enabled := True;
  tmrDelay.Enabled := True;
  tmrChangeCategory.Enabled := True;
end;

procedure TDMConnector.DataModuleDestroy(Sender: TObject);
begin
  FConnector.Free;
end;

procedure TDMConnector.Timer1Timer(Sender: TObject);
var
  lNow10, lNow21: TDateTime;
begin
  Timer1.Enabled := False;
  try
    lNow10 := Now;
    lNow10 := EncodeDateTime(YearOf(lNow10), MonthOf(lNow10), DayOf(lNow10), 8, 0, 0, 0);
    lNow21 := EncodeDateTime(YearOf(lNow10), MonthOf(lNow10), DayOf(lNow10), 21, 0, 0, 0);
    if (lNow10 < Now) and (lNow21 > Now) then
    begin
      FConnector.CheckAndSendAllMessages;
    end;
  finally
    Timer1.Enabled := True;
  end;
end;

procedure TDMConnector.tmrChangeCategoryTimer(Sender: TObject);
begin
  TTimer(Sender).Enabled := False;
  try
    TTimer(Sender).Interval := 3600000;
    FConnector.CheckAndChangeCategoryes;
  finally
    TTimer(Sender).Enabled := True;
  end;
end;

procedure TDMConnector.tmrDelayTimer(Sender: TObject);
begin
  TTimer(Sender).Enabled := False;
  try
    TTimer(Sender).Interval := 60000;
    FConnector.CheckAndExecDelayOpps;
  finally
    TTimer(Sender).Enabled := True;
  end;
end;

procedure TDMConnector.tmrSendSMSTomorrowTimer(Sender: TObject);
var
  lNow16: TDateTime;
begin
  TTimer(Sender).Enabled := False;
  try
    TTimer(Sender).Interval := 300000;
    lNow16 := Now;
    lNow16 := EncodeDateTime(YearOf(lNow16), MonthOf(lNow16), DayOf(lNow16), 9, 0, 0, 0);
    if lNow16 < Now then
    begin
//      FConnector.CheckAndSendOppsTomorrow;
      FConnector.CheckAndSendLostClients;
      FConnector.LastSMSDate := Now;
    end;
  finally
    TTimer(Sender).Enabled := True;
  end;
end;

{ TOzmaConnector }

procedure TOzmaConnector.AddContactsFromFile(AFilename: string);
var
  lData: TBytesStream;
  lStr, lParams: TStringList;
  i: Integer;
  lName, lSurname, lPhone: string;
  lJSONObject, lObjInsert, lObjEntity, lObjEntries : TJSONObject;
  lJSONValue: TJSONValue;
  lArrayOperations: TJSONArray;
  lReqBytes: TBytesStream;
  lRespBytes: TBytesStream;
  lURL: string;
  lContentType: string;
  lBytes: TBytes;
  lClientID: Int64;
  lNotNameCount: Integer;
begin
  lData := TBytesStream.Create;
  lStr := TStringList.Create;
  lParams := TStringList.Create;
  lNotNameCount := 12;
  try
    lParams.LineBreak := ';';
    lData.LoadFromFile(AFilename);
    lStr.Text := StringOf(lData.Bytes);
    for i := 0 to lStr.Count - 1 do
    begin
      try
        Authorize;
        lParams.Text := lStr.Strings[i];
        lName := lParams.Strings[3];
        lSurname := lParams.Strings[4];
        if (lName = '') and (lParams[2] <> '') then
        begin
          lName := lParams[2];
          lSurname := lParams[2];
          if Pos(' ', lName) > 0 then
          begin
            lName := Copy(lName, 1, Pos(' ', lName) - 1);
            lSurname := Copy(lSurname, Pos(' ', lSurname) + 1);
            if lSurname = '' then
            begin
              lSurname := lName;
            end;
          end;
        end;
        if lName = '' then
        begin
          lName := 'Безымянный' + lNotNameCount.ToString;
          Inc(lNotNameCount);
        end;
        lPhone := lParams[18];
        if (lPhone.Length = 11) and (lPhone[1] = '7') then
        begin
          lPhone := '+' + lPhone;
        end
        else
        if (lPhone.Length >= 11) and (lPhone[1] = '8') then
        begin
          lPhone[1] := '7';
          lPhone := '+' + lPhone;
        end
        else
        if ((lPhone.Length = 10)) then
        begin
          lPhone := '+7' + lPhone;
        end;

        lJSONObject := nil;
        try
          // Слздаем структуру
          lJSONObject := TJSONObject.Create;
          lArrayOperations := TJSONArray.Create;
          lObjInsert := TJSONObject.Create;
          lArrayOperations.Add(lObjInsert);
          lJSONObject.AddPair('operations', lArrayOperations);

          // заполняем
          lObjInsert.AddPair('type', 'insert');

          lObjEntity := TJSONObject.Create;
          lObjEntity.AddPair('schema', 'base');
          lObjEntity.AddPair('name', 'people');

          lObjInsert.AddPair('entity', lObjEntity);

          lObjEntries := TJSONObject.Create;
          lObjEntries.AddPair('is_client', 'true');
          lObjEntries.AddPair('first_name', lName);
          lObjEntries.AddPair('last_name', lSurname);

          lObjInsert.AddPair('entries', lObjEntries);

          lClientID := SendInsertTransaction(lJSONObject);
          FreeAndNil(lJSONObject);
          if lClientID = 0 then
          begin
            FIsAuthorize := False;
            Continue;
          end;

          if lPhone <> '' then
          begin
            // Слздаем структуру
            lJSONObject := TJSONObject.Create;
            lArrayOperations := TJSONArray.Create;
            lObjInsert := TJSONObject.Create;
            lArrayOperations.Add(lObjInsert);
            lJSONObject.AddPair('operations', lArrayOperations);

            // заполняем
            lObjInsert.AddPair('type', 'insert');

            lObjEntity := TJSONObject.Create;
            lObjEntity.AddPair('schema', 'base');
            lObjEntity.AddPair('name', 'communication_ways');

            lObjInsert.AddPair('entity', lObjEntity);

            lObjEntries := TJSONObject.Create;
            lObjEntries.AddPair('contact', lClientID.ToString);
            lObjEntries.AddPair('data', lPhone);
            lObjEntries.AddPair('type', 'Телефон');

            lObjInsert.AddPair('entries', lObjEntries);

            lClientID := SendInsertTransaction(lJSONObject);
            if lClientID = 0 then
            begin
              FIsAuthorize := False;
              Continue;
            end;
          end;

        finally
          FreeAndNil(lJSONObject);
        end;
      except
        FIsAuthorize := False;
      end;
    end;
  finally
    lData.Free;
    lStr.Free;
    lParams.Free;
  end;
end;

function TOzmaConnector.Authorize: Boolean;
var
  lURL: string;
  lRespBytes: TBytesStream;
  lParams: TStringList;
  lBytesResp: TBytes;
begin
  Result := FIsAuthorize;
  if not FIsAuthorize then
  begin
    lRespBytes := TBytesStream.Create;
    lParams := TStringList.Create;
    try
      lURL := cnstAuthURL + '/auth/realms/default/protocol/openid-connect/token';
      lParams.Values['client_id'] := cnstClientID;
      lParams.Values['client_secret'] := cnstSecret;
      lParams.Values['username'] := cnstUsername;
      lParams.Values['password'] := cnstPassword;
      lParams.Values['grant_type'] := 'password';
      if FHTTP.Request.CustomHeaders.IndexOfName('Authorization') >= 0 then
      begin
        FHTTP.Request.CustomHeaders.Delete(FHTTP.Request.CustomHeaders.IndexOfName('Authorization'));
      end;
      FHTTP.Request.ContentType := 'application/x-www-form-urlencoded';
      FHTTP.Post(lURL, lParams, lRespBytes);
      lBytesResp := lRespBytes.Bytes;
      SetLength(lBytesResp, lRespBytes.Size);
      Result := ImportNewTokens(StringOf(lBytesResp));
    finally
      FIsAuthorize := Result;
      lRespBytes.Free;
      lParams.Free;
    end;
  end
  else
  if MinutesBetween(FLastDateAuth, Now) > 2 then
  begin
    lRespBytes := TBytesStream.Create;
    lParams := TStringList.Create;
    try
      lURL := cnstAuthURL + '/auth/realms/default/protocol/openid-connect/token';
      lParams.Values['client_id'] := cnstClientID;
      lParams.Values['client_secret'] := cnstSecret;
      lParams.Values['refresh_token'] := FRefreshToken;
      lParams.Values['grant_type'] := 'refresh_token';
      if FHTTP.Request.CustomHeaders.IndexOfName('Authorization') >= 0 then
      begin
        FHTTP.Request.CustomHeaders.Delete(FHTTP.Request.CustomHeaders.IndexOfName('Authorization'));
      end;
      FHTTP.Request.ContentType := 'application/x-www-form-urlencoded';
      FHTTP.Post(lURL, lParams, lRespBytes);
      lBytesResp := lRespBytes.Bytes;
      SetLength(lBytesResp, lRespBytes.Size);
      Result := ImportNewTokens(StringOf(lBytesResp));
      FLastDateAuth := Now;
    finally
      FIsAuthorize := Result;
      lRespBytes.Free;
      lParams.Free;
    end;
  end;
end;

constructor TOzmaConnector.Create;
begin
  FHTTP := TCustIdHTTP.Create(nil);
  FHTTP.HandleRedirects := True;
  FHTTP.AllowCookies := True;
  FIsAuthorize := False;
  FCurrentToken := '';
  FRefreshToken := '';
  FLastDateAuth := 0;
  FMessages := nil;
end;

procedure TOzmaConnector.CreateDelayTask(AOrgID, AOppID: Int64; APriority: Integer);
var
  lData: TBytesStream;
  lStr, lParams: TStringList;
  i: Integer;
  lName, lSurname, lPhone: string;
  lJSONObject, lObjInsert, lObjEntity, lObjEntries : TJSONObject;
  lJSONValue: TJSONValue;
  lArrayOperations: TJSONArray;
  lClientID: Int64;
begin
  lJSONObject := nil;
  try
    // Слздаем структуру
    lJSONObject := TJSONObject.Create;
    lArrayOperations := TJSONArray.Create;
    lObjInsert := TJSONObject.Create;
    lArrayOperations.Add(lObjInsert);
    lJSONObject.AddPair('operations', lArrayOperations);

    // заполняем
    lObjInsert.AddPair('type', 'insert');

    lObjEntity := TJSONObject.Create;
    lObjEntity.AddPair('schema', 'pm');
    lObjEntity.AddPair('name', 'actions');

    lObjInsert.AddPair('entity', lObjEntity);

    lObjEntries := TJSONObject.Create;
    lObjEntries.AddPair('subject', AnsiToUtf8('Автоматический перенос заявки на завтра'));
    lObjEntries.AddPair('type', '1');
    lObjEntries.AddPair('due_date', FormatDateTime('yyyy-mm-dd', Now));
    lObjEntries.AddPair('contact', AOrgID.ToString);
    lObjEntries.AddPair('start_date', FormatDateTime('yyyy-mm-dd', Now));
    lObjEntries.AddPair('opportunity', AOppID.ToString);
    lObjEntries.AddPair('priority', APriority.ToString);

    lObjInsert.AddPair('entries', lObjEntries);

    lClientID := SendInsertTransaction(lJSONObject);
    FreeAndNil(lJSONObject);
    // Слздаем структуру
    lJSONObject := TJSONObject.Create;
    lArrayOperations := TJSONArray.Create;
    lObjInsert := TJSONObject.Create;
    lArrayOperations.Add(lObjInsert);
    lJSONObject.AddPair('operations', lArrayOperations);

    // заполняем
    lObjInsert.AddPair('type', 'update');
    lObjInsert.AddPair('id', AOppID.ToString);

    lObjEntity := TJSONObject.Create;
    lObjEntity.AddPair('schema', 'crm');
    lObjEntity.AddPair('name', 'opportunities');

    lObjInsert.AddPair('entity', lObjEntity);

    lObjEntries := TJSONObject.Create;
    lObjEntries.AddPair('delay_task', lClientID.ToString);

    lObjInsert.AddPair('entries', lObjEntries);

    SendUpdateTransaction(lJSONObject);
  finally
    FreeAndNil(lJSONObject);
  end;
end;

procedure TOzmaConnector.CreateTaskByLostClient(AOrgID: Int64;
  AOrgName: string);
var
  lData: TBytesStream;
  lStr, lParams: TStringList;
  i: Integer;
  lName, lSurname, lPhone: string;
  lJSONObject, lObjInsert, lObjEntity, lObjEntries : TJSONObject;
  lJSONValue: TJSONValue;
  lArrayOperations: TJSONArray;
  lClientID: Int64;
begin
  lJSONObject := nil;
  try
    // Слздаем структуру
    lJSONObject := TJSONObject.Create;
    lArrayOperations := TJSONArray.Create;
    lObjInsert := TJSONObject.Create;
    lArrayOperations.Add(lObjInsert);
    lJSONObject.AddPair('operations', lArrayOperations);

    // заполняем
    lObjInsert.AddPair('type', 'insert');

    lObjEntity := TJSONObject.Create;
    lObjEntity.AddPair('schema', 'pm');
    lObjEntity.AddPair('name', 'actions');

    lObjInsert.AddPair('entity', lObjEntity);

    lObjEntries := TJSONObject.Create;
    lObjEntries.AddPair('subject', AnsiToUtf8('Потеряный клиент: '
      + AOrgName));
    lObjEntries.AddPair('type', '1');
    lObjEntries.AddPair('due_date', FormatDateTime('yyyy-mm-dd', Now));
    lObjEntries.AddPair('contact', AOrgID.ToString);
    lObjEntries.AddPair('start_date', FormatDateTime('yyyy-mm-dd', Now));
    lObjEntries.AddPair('priority', '2');

    lObjInsert.AddPair('entries', lObjEntries);

    lClientID := SendInsertTransaction(lJSONObject);
  finally
    FreeAndNil(lJSONObject);
  end;
end;

destructor TOzmaConnector.Destroy;
begin
  FHTTP.Free;
  inherited;
end;

function TOzmaConnector.GetAllMessages: TMessages;
var
  lURL: string;
  lRespBytes: TBytesStream;
  lBytes: TBytes;
  i: Integer;
  lJSONObject: TJSONObject;
  lJSONValue: TJSONValue;
  lJSONArray, lJSONArrayValue: TJSONArray;  
  lEncoding: TEncoding;
begin
  Authorize;
  Result := nil;
  lRespBytes := TBytesStream.Create;
  lJsonValue := nil;
  lEncoding := nil;
  try
    lURL := cnstURL + '/views/by_name/sms/get_messages_for_send/entries';
    FHTTP.Get(lURL, lRespBytes);
    lBytes := lRespBytes.Bytes;
    SetLength(lBytes, lRespBytes.Size);
    lJsonValue :=  TJSONObject.ParseJSONValue(StringOf(lBytes));
    if Assigned(lJsonValue) then
    begin
      if lJsonValue is TJSONObject then
      begin
        lJsonObject := TJSONObject(lJsonValue);
        lJsonObject := lJsonObject.GetValue<TJSONObject>('result');
        lJSONArray := lJsonObject.GetValue<TJSONArray>('rows');
        if lJSONArray.Count > 0 then
        begin    
          SetLength(Result, lJSONArray.Count);
          lEncoding := TEncoding.Create; 
          for i := 0 to lJSONArray.Count - 1 do
          begin
            lJsonObject := TJSONObject(lJSONArray.Items[i]);
            lJSONArrayValue := lJsonObject.GetValue<TJSONArray>('values');
            if lJSONArrayValue.Count = 4 then
            begin
              Result[i].ID := lJSONArrayValue.Items[0].GetValue<Int64>('value');
              Result[i].Text := Utf8ToAnsi(lJSONArrayValue.Items[1].GetValue<string>('value'));
//              Result[i].Text := StringOf(lEncoding.Convert(lEncoding.UTF8, lEncoding.GetEncoding(1252)
//                , BytesOf(lJSONArrayValue.Items[1].GetValue<string>('value'))));
              Result[i].Text := StringReplace(Result[i].Text, ' ', '+', [rfReplaceAll]);
              Result[i].Phone := StringOf(lEncoding.Convert(lEncoding.UTF8, lEncoding.GetEncoding(1252)
                , BytesOf(lJSONArrayValue.Items[2].GetValue<string>('value'))));
              Result[i].Phone := TransformPhone(Result[i].Phone);
            end;
          end;
        end;
      end;
    end;
  finally
    lRespBytes.Free;
    FreeAndNil(lJsonValue);  
    lEncoding.Free;
  end;
end;

procedure TOzmaConnector.GetAndUpdateCategoryForClients;
var
  lURL: string;
  lRespBytes: TBytesStream;
  lBytes: TBytes;
  i: Integer;
  lJSONObjCategoryes, lJSONObject, lObjInsert, lObjEntity, lObjEntries: TJSONObject;
  lJSONValue: TJSONValue;
  lJSONArray, lJSONArrayValue, lArrayOperations: TJSONArray;
  lEncoding: TEncoding;
  lFS: TFormatSettings;
begin
  Authorize;
  lRespBytes := TBytesStream.Create;
  lJsonValue := nil;
  lEncoding := nil;
  lJSONObject := nil;
  try
    lFS.DecimalSeparator := '.';
    lURL := cnstURL + '/views/by_name/sms/get_clients_for_change_category/entries';
    FHTTP.Get(lURL, lRespBytes);
    lBytes := lRespBytes.Bytes;
    SetLength(lBytes, lRespBytes.Size);
    lJsonValue :=  TJSONObject.ParseJSONValue(StringOf(lBytes));
    if Assigned(lJsonValue) then
    begin
      if lJsonValue is TJSONObject then
      begin
        lJSONObjCategoryes := TJSONObject(lJsonValue);
        lJSONObjCategoryes := lJSONObjCategoryes.GetValue<TJSONObject>('result');
        lJSONArray := lJSONObjCategoryes.GetValue<TJSONArray>('rows');
        for i := 0 to lJSONArray.Count - 1 do
        begin
          lJSONObjCategoryes := TJSONObject(lJSONArray.Items[i]);
          lJSONArrayValue := lJSONObjCategoryes.GetValue<TJSONArray>('values');
          if lJSONArrayValue.Count = 4 then
          begin
            // Слздаем структуру
            lJSONObject := TJSONObject.Create;
            lArrayOperations := TJSONArray.Create;
            lObjInsert := TJSONObject.Create;
            lArrayOperations.Add(lObjInsert);
            lJSONObject.AddPair('operations', lArrayOperations);

            // заполняем
            lObjInsert.AddPair('type', 'update');
            lObjInsert.AddPair('id', lJSONArrayValue.Items[0].GetValue<string>('value'));

            lObjEntity := TJSONObject.Create;
            lObjEntity.AddPair('schema', 'base');
            lObjEntity.AddPair('name', 'organizations');

            lObjInsert.AddPair('entity', lObjEntity);

            lObjEntries := TJSONObject.Create;
            lObjEntries.AddPair('category', lJSONArrayValue.Items[1].GetValue<string>('value'));
            lObjEntries.AddPair('volume_year', IntToStr(Trunc(lJSONArrayValue.Items[2].GetValue<Double>('value'))));

            lObjInsert.AddPair('entries', lObjEntries);

            SendUpdateTransaction(lJSONObject);
          end;
        end;
      end;
    end;
  finally
    lRespBytes.Free;
    FreeAndNil(lJsonValue);
    FreeAndNil(lJSONObject);
  end;
end;

function TOzmaConnector.GetDelayOpportunities: TDelayOpportunities;
var
  lURL: string;
  lRespBytes: TBytesStream;
  lBytes: TBytes;
  i: Integer;
  lJSONObject: TJSONObject;
  lJSONValue: TJSONValue;
  lJSONArray, lJSONArrayValue: TJSONArray;
  lEncoding: TEncoding;
  lFS: TFormatSettings;
begin
  Authorize;
  Result := nil;
  lRespBytes := TBytesStream.Create;
  lJsonValue := nil;
  lEncoding := nil;
  try
    lFS.DecimalSeparator := '.';
    lURL := cnstURL + '/views/by_name/sms/get_delay_opportunities/entries';
    FHTTP.Get(lURL, lRespBytes);
    lBytes := lRespBytes.Bytes;
    SetLength(lBytes, lRespBytes.Size);
    lJsonValue :=  TJSONObject.ParseJSONValue(StringOf(lBytes));
    if Assigned(lJsonValue) then
    begin
      if lJsonValue is TJSONObject then
      begin
        lJsonObject := TJSONObject(lJsonValue);
        lJsonObject := lJsonObject.GetValue<TJSONObject>('result');
        lJSONArray := lJsonObject.GetValue<TJSONArray>('rows');
        if lJSONArray.Count > 0 then
        begin
          SetLength(Result, lJSONArray.Count);
          for i := 0 to lJSONArray.Count - 1 do
          begin
            lJsonObject := TJSONObject(lJSONArray.Items[i]);
            lJSONArrayValue := lJsonObject.GetValue<TJSONArray>('values');
            if lJSONArrayValue.Count = 10 then
            begin
              Result[i].OrgID := lJSONArrayValue.Items[0].GetValue<Int64>('value');
              Result[i].OrgName := Utf8ToAnsi(lJSONArrayValue.Items[1].GetValue<string>('value'));
              Result[i].OrgShortName := Utf8ToAnsi(lJSONArrayValue.Items[2].GetValue<string>('value'));
              Result[i].OrgCategory := Utf8ToAnsi(lJSONArrayValue.Items[3].GetValue<string>('value'));
              Result[i].OrgCategoryType := Utf8ToAnsi(lJSONArrayValue.Items[4].GetValue<string>('value'));
              Result[i].PersonName := Utf8ToAnsi(lJSONArrayValue.Items[5].GetValue<string>('value'));
              Result[i].PersonPhone := TransformPhone(lJSONArrayValue.Items[6].GetValue<string>('value'));
              Result[i].MinutesDelay := Trunc(StrToFloat(lJSONArrayValue.Items[7].GetValue<string>('value'), lFS));
              Result[i].DelayTask := StrToInt64Def(lJSONArrayValue.Items[8].GetValue<string>('value'), 0);
              Result[i].OppID := lJSONArrayValue.Items[9].GetValue<Int64>('value');
            end;
          end;
        end;
      end;
    end;
  finally
    lRespBytes.Free;
    FreeAndNil(lJsonValue);
  end;
end;

function TOzmaConnector.GetLossClients: TLossClients;
var
  lURL: string;
  lRespBytes: TBytesStream;
  lBytes: TBytes;
  i: Integer;
  lJSONObject: TJSONObject;
  lJSONValue: TJSONValue;
  lJSONArray, lJSONArrayValue: TJSONArray;
  lEncoding: TEncoding;
begin
  Authorize;
  Result := nil;
  lRespBytes := TBytesStream.Create;
  lJsonValue := nil;
  lEncoding := nil;
  try
    lURL := cnstURL + '/views/by_name/sms/get_loss_clients/entries';
    FHTTP.Get(lURL, lRespBytes);
    lBytes := lRespBytes.Bytes;
    SetLength(lBytes, lRespBytes.Size);
    lJsonValue :=  TJSONObject.ParseJSONValue(StringOf(lBytes));
    if Assigned(lJsonValue) then
    begin
      if lJsonValue is TJSONObject then
      begin
        lJsonObject := TJSONObject(lJsonValue);
        lJsonObject := lJsonObject.GetValue<TJSONObject>('result');
        lJSONArray := lJsonObject.GetValue<TJSONArray>('rows');
        if lJSONArray.Count > 0 then
        begin
          SetLength(Result, lJSONArray.Count);
          lEncoding := TEncoding.Create;
          for i := 0 to lJSONArray.Count - 1 do
          begin
            lJsonObject := TJSONObject(lJSONArray.Items[i]);
            lJSONArrayValue := lJsonObject.GetValue<TJSONArray>('values');
            if lJSONArrayValue.Count = 9 then
            begin
              Result[i].ID := lJSONArrayValue.Items[0].GetValue<Int64>('value');
              Result[i].FIO := Utf8ToAnsi(lJSONArrayValue.Items[1].GetValue<string>('value'));
              Result[i].OrgName := Utf8ToAnsi(lJSONArrayValue.Items[2].GetValue<string>('value'));
              Result[i].Phone := TransformPhone(lJSONArrayValue.Items[3].GetValue<string>('value'));
              Result[i].Category := Utf8ToAnsi(lJSONArrayValue.Items[4].GetValue<string>('value'));
              Result[i].CategoryType := Utf8ToAnsi(lJSONArrayValue.Items[5].GetValue<string>('value'));
              Result[i].CurrVolume := lJSONArrayValue.Items[6].GetValue<Integer>('value');
              Result[i].Price := lJSONArrayValue.Items[7].GetValue<Currency>('value');
              Result[i].DayLoss := Trunc(lJSONArrayValue.Items[8].GetValue<Double>('value'));

            end;
          end;
        end;
      end;
    end;
  finally
    lRespBytes.Free;
    FreeAndNil(lJsonValue);
    lEncoding.Free;
  end;
end;

function TOzmaConnector.GetMessagesForClientTomorrow: TOpportunities;
var
  lURL: string;
  lRespBytes: TBytesStream;
  lBytes: TBytes;
  i: Integer;
  lJSONObject: TJSONObject;
  lJSONValue: TJSONValue;
  lJSONArray, lJSONArrayValue: TJSONArray;
begin
  Authorize;
  Result := nil;
  lRespBytes := TBytesStream.Create;
  lJsonValue := nil;
  try
    lURL := cnstURL + '/views/by_name/sms/get_opportunities_client_by_date/entries?day_search="'
      + FormatDateTime('yyyy-mm-dd', Tomorrow) + '"';
    FHTTP.Get(lURL, lRespBytes);
    lBytes := lRespBytes.Bytes;
    SetLength(lBytes, lRespBytes.Size);
    lJsonValue :=  TJSONObject.ParseJSONValue(StringOf(lBytes));
    if Assigned(lJsonValue) then
    begin
      if lJsonValue is TJSONObject then
      begin
        lJsonObject := TJSONObject(lJsonValue);
        lJsonObject := lJsonObject.GetValue<TJSONObject>('result');
        lJSONArray := lJsonObject.GetValue<TJSONArray>('rows');
        if lJSONArray.Count > 0 then
        begin    
          SetLength(Result, lJSONArray.Count);
          for i := 0 to lJSONArray.Count - 1 do
          begin
            lJsonObject := TJSONObject(lJSONArray.Items[i]);
            lJSONArrayValue := lJsonObject.GetValue<TJSONArray>('values');
            if lJSONArrayValue.Count = 6 then
            begin
              Result[i].ID := lJSONArrayValue.Items[0].GetValue<Int64>('value');
              Result[i].CurrVolume := lJSONArrayValue.Items[1].GetValue<Integer>('value'); 
              Result[i].VolumePrice := lJSONArrayValue.Items[2].GetValue<Currency>('value');
              Result[i].Operation := Utf8ToAnsi(lJSONArrayValue.Items[3].GetValue<string>('value'));
              Result[i].PaymentTool := Utf8ToAnsi(lJSONArrayValue.Items[4].GetValue<string>('value'));
              Result[i].Phone := TransformPhone(lJSONArrayValue.Items[5].GetValue<string>('value'));
            end;
          end;
        end;
      end;
    end;
  finally
    lRespBytes.Free;
    FreeAndNil(lJsonValue);
  end;
end;

function TOzmaConnector.ImportNewTokens(AJSON: string): Boolean;
var
  lJsonValue: TJSONValue;
  lJsonObject: TJSONObject;
  lTokenStr: string;
begin
  Result := False;
  lJsonValue :=  TJSONObject.ParseJSONValue(AJSON);
  try
    if Assigned(lJsonValue) then
    begin
      if lJsonValue is TJSONObject then
      begin
        lJsonObject := TJSONObject(lJsonValue);
        if lJsonObject.FindValue('access_token') <> nil then
        begin
          lTokenStr := lJsonObject.GetValue<string>('access_token');
          Result := False;
          if lTokenStr <> '' then
          begin
            FCurrentToken := lTokenStr;
            FHTTP.Request.CustomHeaders.AddValue('Authorization', 'Bearer ' + FCurrentToken);
            Result := True;
          end;
        end;
        if Result and (lJsonObject.FindValue('refresh_token') <> nil) then
        begin
          lTokenStr := lJsonObject.GetValue<string>('refresh_token');
          Result := False;
          if lTokenStr <> '' then
          begin
            FRefreshToken := lTokenStr;
            Result := True;
          end;
        end;
      end;
    end;
  finally
    FreeAndNil(lJsonValue);
  end;
end;

procedure TOzmaConnector.MessageCheckSending(AMessageID: Int64);
var
  lJSONObject, lObjUpdate, lObjEntity, lObjEntries : TJSONObject;
  lJSONValue: TJSONValue;
  lArrayOperations: TJSONArray;
  lURL: string;
  lReqBytes, lRespBytes: TBytesStream;
  lBytes: TBytes;
  lContentType: string;
  lID: Integer;
begin
  lID := 0;
  // Создаем структуру
  lJSONObject := nil;
  try
    lJSONObject := TJSONObject.Create;
    lArrayOperations := TJSONArray.Create;
    lObjUpdate := TJSONObject.Create;
    lArrayOperations.Add(lObjUpdate);
    lJSONObject.AddPair('operations', lArrayOperations);

    // заполняем
    lObjUpdate.AddPair('type', 'update');
    lObjUpdate.AddPair('id', AMessageID.ToString);

    lObjEntity := TJSONObject.Create;
    lObjEntity.AddPair('schema', 'sms');
    lObjEntity.AddPair('name', 'messages_for_send');

    lObjUpdate.AddPair('entity', lObjEntity);

    lObjEntries := TJSONObject.Create;
    lObjEntries.AddPair('is_done', 'true');

    lObjUpdate.AddPair('entries', lObjEntries);
    if not SendUpdateTransaction(lJSONObject) then
    begin
      raise Exception.Create('Message [' + AMessageID.ToString + '] not ready to send!');
    end;
  finally
    FreeAndNil(lJSONObject);
  end;
end;

procedure TOzmaConnector.ResendOpportunityTomorrow(AOppID, ADelayTaskID: Int64);
var
  lJSONObject, lObjUpdate, lObjEntity, lObjEntries : TJSONObject;
  lJSONValue: TJSONValue;
  lArrayOperations: TJSONArray;
  lURL: string;
  lReqBytes, lRespBytes: TBytesStream;
  lBytes: TBytes;
  lContentType: string;
  lID: Integer;
begin
  lID := 0;
  // Создаем структуру
  lJSONObject := nil;
  try
    lJSONObject := TJSONObject.Create;
    lArrayOperations := TJSONArray.Create;
    lObjUpdate := TJSONObject.Create;
    lArrayOperations.Add(lObjUpdate);
    lJSONObject.AddPair('operations', lArrayOperations);

    // заполняем
    lObjUpdate.AddPair('type', 'update');
    lObjUpdate.AddPair('id', AOppID.ToString);

    lObjEntity := TJSONObject.Create;
    lObjEntity.AddPair('schema', 'crm');
    lObjEntity.AddPair('name', 'opportunities');

    lObjUpdate.AddPair('entity', lObjEntity);

    lObjEntries := TJSONObject.Create;
    lObjEntries.AddPair('start_date', FormatDateTime('yyyy-mm-dd', Now + 1));
    if ADelayTaskID >= 0 then
    begin
      lObjEntries.AddPair('delay_task', ADelayTaskID.ToString);
    end;

    lObjUpdate.AddPair('entries', lObjEntries);
    if not SendUpdateTransaction(lJSONObject) then
    begin
      raise Exception.Create('opportunities [' + AOppID.ToString + '] not resend tomorrow!');
    end;
  finally
    FreeAndNil(lJSONObject);
  end;
end;

function TOzmaConnector.SendInsertTransaction(AObject: TJSONObject): Int64;
var
  lJSONObject : TJSONObject;
  lJSONValue: TJSONValue;
  lArrayOperations: TJSONArray;
  lReqBytes: TBytesStream;
  lRespBytes: TBytesStream;
  lURL: string;
  lBytes: TBytes;
  lEncoding: TEncoding;
  lCount: Integer;
begin
  Result := 0;
  lReqBytes := nil;
  lRespBytes := TBytesStream.Create;
  lJsonValue := nil;
  lEncoding := TEncoding.Create;
  try
    lReqBytes := TBytesStream.Create(BytesOf(AnsiToUtf8(AObject.ToString)));
//    lReqBytes := TBytesStream.Create(lEncoding.Convert(lEncoding.Default
//      , lEncoding.UTF8, BytesOf(AObject.ToString)));
    lURL := cnstURL + '/transaction';
    FHTTP.Request.ContentType := 'application/json';
    lCount := 1;
    while lCount < 3 do
    begin
      try
        FHTTP.Post(lURL, lReqBytes, lRespBytes);
        Break;
      except
        FreeAndNil(FHTTP);
        Inc(lCount);
        Sleep(1000);
        FHTTP := TCustIdHTTP.Create(nil);
        Authorize;
      end;
    end;
    lBytes := lRespBytes.Bytes;
    SetLength(lBytes, lRespBytes.Size);
    lJsonValue :=  TJSONObject.ParseJSONValue(StringOf(lBytes));
    if Assigned(lJsonValue) then
    begin
      if lJsonValue is TJSONObject then
      begin
        lJsonObject := TJSONObject(lJsonValue);
        lArrayOperations := lJsonObject.GetValue<TJSONArray>('results');
        if lArrayOperations.Count = 1 then
        begin
          lJsonObject := TJSONObject(lArrayOperations.Items[0]);
          if (lJsonObject.FindValue('type') <> nil) and (lJsonObject.GetValue<string>('type') = 'insert') then
          begin
            Result := lJsonObject.GetValue<Integer>('id');
          end;
        end;
      end;
    end;
  finally
    lRespBytes.Free;
    FreeAndNil(lReqBytes);
    lEncoding.Free;
    FreeAndNil(lJsonValue);
  end;
end;

function TOzmaConnector.SendUpdateTransaction(AObject: TJSONObject): Boolean;
var
  lJSONObject : TJSONObject;
  lJSONValue: TJSONValue;
  lArrayOperations: TJSONArray;
  lReqBytes: TBytesStream;
  lRespBytes: TBytesStream;
  lURL: string;
  lBytes: TBytes;
  lEncoding: TEncoding;
  lCount: Integer;
begin
  Result := False;
  lReqBytes := nil;
  lRespBytes := TBytesStream.Create;
  lJsonValue := nil;
  lEncoding := TEncoding.Create;
  try
//    lReqBytes := TBytesStream.Create(BytesOf(AObject.ToString));
    lReqBytes := TBytesStream.Create(BytesOf(AnsiToUtf8(AObject.ToString)));
    lURL := cnstURL + '/transaction';
    FHTTP.Request.ContentType := 'application/json';
    lCount := 1;
    while lCount < 3 do
    begin
      try
        FHTTP.Post(lURL, lReqBytes, lRespBytes);
        Break;
      except
        FreeAndNil(FHTTP);
        Inc(lCount);
        Sleep(1000);
        FHTTP := TCustIdHTTP.Create(nil);
        Authorize;
      end;
    end;
    lBytes := lRespBytes.Bytes;
    SetLength(lBytes, lRespBytes.Size);
    lJsonValue :=  TJSONObject.ParseJSONValue(StringOf(lBytes));
    if Assigned(lJsonValue) then
    begin
      if lJsonValue is TJSONObject then
      begin
        lJsonObject := TJSONObject(lJsonValue);
        lArrayOperations := lJsonObject.GetValue<TJSONArray>('results');
        if lArrayOperations.Count = 1 then
        begin
          lJsonObject := TJSONObject(lArrayOperations.Items[0]);
          Result := (lJsonObject.FindValue('type') <> nil) and (lJsonObject.GetValue<string>('type') = 'update');
        end;
      end;
    end;
  finally
    lRespBytes.Free;
    FreeAndNil(lReqBytes);
    lEncoding.Free;
    FreeAndNil(lJsonValue);
  end;
end;

procedure TOzmaConnector.SetLossClientNow(AClientID: Int64);
var
  lJSONObject, lObjUpdate, lObjEntity, lObjEntries : TJSONObject;
  lJSONValue: TJSONValue;
  lArrayOperations: TJSONArray;
  lURL: string;
  lReqBytes, lRespBytes: TBytesStream;
  lBytes: TBytes;
  lContentType: string;
  lID: Integer;
begin
  lID := 0;
  // Создаем структуру
  lJSONObject := nil;
  try
    lJSONObject := TJSONObject.Create;
    lArrayOperations := TJSONArray.Create;
    lObjUpdate := TJSONObject.Create;
    lArrayOperations.Add(lObjUpdate);
    lJSONObject.AddPair('operations', lArrayOperations);

    // заполняем
    lObjUpdate.AddPair('type', 'update');
    lObjUpdate.AddPair('id', AClientID.ToString);

    lObjEntity := TJSONObject.Create;
    lObjEntity.AddPair('schema', 'base');
    lObjEntity.AddPair('name', 'organizations');

    lObjUpdate.AddPair('entity', lObjEntity);

    lObjEntries := TJSONObject.Create;
    lObjEntries.AddPair('date_send_loss', FormatDateTime('yyyy-mm-dd', Now));

    lObjUpdate.AddPair('entries', lObjEntries);
    if not SendUpdateTransaction(lJSONObject) then
    begin
      raise Exception.Create('organizations [' + AClientID.ToString + '] not set now messages!');
    end;
  finally
    FreeAndNil(lJSONObject);
  end;
end;

function TOzmaConnector.TransformPhone(ASourcePhone: string): string;
begin
  Result := ASourcePhone;
  if ASourcePhone <> '' then
  begin  
    Result := StringReplace(Result, ' ', '', [rfReplaceAll]);
    Result := StringReplace(Result, '(', '', [rfReplaceAll]);
    Result := StringReplace(Result, ')', '', [rfReplaceAll]);   
    Result := StringReplace(Result, '+', '', [rfReplaceAll]); 
    Result := StringReplace(Result, '-', '', [rfReplaceAll]);
    if Length(Result) = 10 then
    begin
      Result := '7' + Result;
    end
    else
    if (Length(Result) = 11) and (Result[1] = '8') then
    begin
      Result[1] := '7';
    end;
  end;
end;

procedure TOzmaConnector.Unauthorize;
begin
  FIsAuthorize := False;
end;

end.
