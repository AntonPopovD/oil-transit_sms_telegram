program AppSMS;

uses
  FastMM4,
  Vcl.Forms,
  uFormMain in 'uFormMain.pas' {Form1},
  uDMServer in 'uDMServer.pas' {DMConnector: TDataModule},
  CustIdHTTP in 'CustIdHTTP.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TDMConnector, DMConnector);
  Application.Run;
end.
