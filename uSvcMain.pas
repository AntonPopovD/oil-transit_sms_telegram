unit uSvcMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.SvcMgr, Vcl.Dialogs,
  Vcl.ExtCtrls;

type
  Tsvc_OIL_SMS_Telegram_Sender = class(TService)
    Timer1: TTimer;
  private
    { Private declarations }
  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;

var
  svc_OIL_SMS_Telegram_Sender: Tsvc_OIL_SMS_Telegram_Sender;

implementation

{$R *.dfm}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  svc_OIL_SMS_Telegram_Sender.Controller(CtrlCode);
end;

function Tsvc_OIL_SMS_Telegram_Sender.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

end.
